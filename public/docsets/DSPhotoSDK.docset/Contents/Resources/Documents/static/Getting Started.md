# Getting Started

Follow the steps below to get started using the DSPhoto SDK.

## Import the Framework

1. Download the framework to a suitable location (the `.framework` file). 
2. Open your project in Xcode.
3. In the project navigator (left side column), select your project.
4. Click on `General`. 
5. Scroll down until the section `Embedded Binaries` appears.
6. Click on the plus sign (`+`).
7. Click on the button entitled `Add Other`.
8. Navigate to the location you placed the framework and select it.
9. When the import dialog appears, mark the option `Copy items if needed`.
10. Click on `Finish`.

## Disable Bitcode

1. In the project navigator (left side column), select your project.
2. Click on `Build Settings`. 
3. Search for the term `bitcode`.
4. When the setting `Enable Bitcode` appears, set it to `NO`. 

## Enable Services

Now, you just need to enable the services. Open your application delegate, the controller that conforms to the protocol `UIApplicationDelegate`, and validate your license using the method `activate:` from the module `DSLicenseService`. See the example below:

```swift
#pragma mark -
#pragma mark - UIApplicationDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    NSString *license = @"YOUR_LICENSE_HERE";
    
    if ([[DSLicenseService sharedInstance] activate:license] == YES) {

        // Valid license. The SDK services are enabled.

    } else {

        // Your license is invalid.

    }

    return YES;

}
```



