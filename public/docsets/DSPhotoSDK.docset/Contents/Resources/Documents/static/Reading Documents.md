# Reading Documents

The module `DSDocumentReader` was designed to extract textual information from documents and match them to a given template.  Follow this guide for an usage demonstration and read the guide `Creating Templates` to learn how to design your own customized templates.

We start creating a simple template to match the name, ID and CPF fields from brazilian driver's licenses. Create a new file named `template_cnh.xml`, copy and paste the code below in it and import to your project.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<template>
    <documentRatio>0.6978</documentRatio>
    <anchors>
        <anchor text="NOME"         x="0.20"      y="0.2506" />
        <anchor text="IDENTIDADE"   x="0.6055"    y="0.3536" />
        <anchor text="CPF"          x="0.5316"    y="0.4466" />
    </anchors>
    <fields>
        <field name="Nome"          x="0.16;0.95" y="0.22;0.32" />
        <field name="DocIdentidade" x="0.48;0.95" y="0.35;0.42" />
        <field name="CPF"           x="0.48;0.75" y="0.45;0.52" />
    </fields>
</template>
```

Having the image and template, it's easy peasy lemon squeezy. You just need to invoke the method `processDocument:templatePath:completion:`. Below we assume that you already have a picture of a document with the perspective adjusted. Observe that the dictionary keys match the attribute name of the template's fields.

```swift
NSString *template = @"template_cnh";
NSString *path = [[NSBundle mainBundle] pathForResource:template ofType:@"xml"];

DSImage *image = /* picture of a document with the perspective adjusted */;

[[DSDocumentReader sharedInstance] processDocument:image templatePath:path completion:^(NSError * _Nullable error, NSDictionary * _Nullable data) {

    if (error == nil) {

        NSString *name = [data objectForKey:@"Nome"];
        NSString *docID = [data objectForKey:@"DocIdentidade"];
        NSString *docCPF = [data objectForKey:@"CPF"];
        
        NSLog(@"%@ %@ %@", name, docID, docCPF);
        
    }

}];
```

The template used in this example is a simplified version, check out the complete ones:

* [Brazilian RG - Back](static/templates/TemplateRGBack.xml)
* [Brazilian RG - Front](static/templates/TemplateRGFront.xml)
* [Brazilian Driver's License](static/templates/TemplateCNH.xml)
* [Brazilian Driver's License - Upper Half](static/templates/TemplateCNHTop.xml)
* [Brazilian Driver's License - Lower Half](static/templates/TemplateCNHBottom.xml)

> **Important Note:**
>
> Note that more anchors and precise positions will most likely yield better results. 

