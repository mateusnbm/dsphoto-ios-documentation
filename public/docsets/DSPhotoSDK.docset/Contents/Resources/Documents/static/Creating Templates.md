# Creating Templates

Templates are XML documents used by the module `DSDocumentReader` to extract textual information from generic documents. You can create templates for any kind of document that have a clear and well known structure.

All templates must contain a single root element named `<template>`. It must have three direct childs:

* `<documentRatio>` Element holding a floating point with the result of the division of the image's height by the image's width.

* `<anchors>` This element has no attributes. It must contain at least one child named `<anchor>` with three attributes, see their descriptions below.

    * `text` The text that represents the anchor.
    * `x` A floating point number that specifies the relative horizontal position of the center of the anchor's bounding box. It must range from 0 to 1, given that 0 represents left-most position of the document and 1 represents the right-most position of it.
    * `y` A floating point number that specifies the relative vertical position of the center of the anchor's bounding box. It must range from 0 to 1, given that 0 represents upper-most position of the document and 1 represents the bottom-most position of it.

* `<fields>`. This element has no attributes. It must contain at least one child named `<field>` with three attributes, see their descriptions below.

    * `name` A string that will be used to identify the information extracted from the document.
    * `x` Two floating point numbers separed by a semicolon. The first one must represent the relative horizontal position of the left-most point of the field's bounding box. The second one must represent the relative horizontal position of the right-most point of the field's bounding box.
    * `y` Two floating point numbers separed by a semicolon. The first one must represent the relative vertical position of the upper-most point of the field's bounding box. The second one must represent the relative vertical positino of the bottom-most point of the field's bounding box.

Now, an example. Suppose you want to create a template to extract the name from the brazilian driver's licenses. Get a good sample of the document and measure the document's width, height, the position of the anchor "Nome" and the position of the center of the field name. Since the templates use relative positions, you can use any unit of measurement.

In this example, we'll use the image below to estimate the positions in pixels.

<img src="static/images/cnh.jpg" width="450" height="310">

From it, we got the following positions:

* Document's size in pixels (600, 414).
* Anchor "Nome", central position in pixels (126, 87).
* Field "Nome", top left corner (97, 86) and the bottom right corner (565, 119) in pixels.

They translate into the following attributes:

* Document's ratio, division result is 0.69.
* Anchor "Nome", relative central position (0.21, 0.2101).
* Field "Nome", relative top left corner (0.1616, 0.2077) and bottom right corner (0.9416, 0.2874).

The resultant XML document should look like this:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<template>
    <documentRatio>0.69</documentRatio>
    <anchors>
        <anchor text="NOME" x="0.21" y="0.2101" />
    </anchors>
    <fields>
        <field name="Nome"  x="0.1616;0.9416" y="0.2077;0.2874" />
    </fields>
</template>
```

For your convenience, we've created the following templates:

* [Brazilian RG - Back](static/templates/TemplateRGBack.xml)
* [Brazilian RG - Front](static/templates/TemplateRGFront.xml)
* [Brazilian Driver's License](static/templates/TemplateCNH.xml)
* [Brazilian Driver's License - Upper Half](static/templates/TemplateCNHTop.xml)
* [Brazilian Driver's License - Lower Half](static/templates/TemplateCNHBottom.xml)

> **Important Note:**
>
> Note that more anchors and precise positions will most likely yield better results. 


