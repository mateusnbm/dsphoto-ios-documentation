# Capturing Images

Learn how to capture high quality images and detect documents using the camera module.

## Present the Camera Controller

The camera module provides an interface for capturing high quality images and detecting documents in them. Start by implementing a function to present the view controller with the desired configuration parameters, see `DSCameraController` for the available properties. An example of such implementation is given below:

```swift
- (void)showCameraController {

    DSCameraController *controller = [[DSCameraController alloc] init];
    
    controller.delegate = self;
    controller.captureMode = DSCameraControllerCaptureModeAutomatic;
    controller.capturePreprocessMode = DSPreprocessModeFast;
    controller.captureDocumentFormat = DSDocumentFormatNone;

    controller.shouldShowScoresView = YES;
    controller.ignoreScoresAfterTimeInterval = 10;

    controller.shouldShowTimedLabel = YES;
    controller.hideTimedLabelAfterTimeInterval = 5;

    controller.shouldIgnoreImageScores = NO;
    controller.acceptableLightValue = 0.6f;
    controller.acceptableFocusValue = 0.6f;
    controller.acceptableShineValue = 0.6f;
    controller.acceptableShadowValue = 0.6f;
    
    [self presentViewController:controller animated:true completion:nil];
    
}
```

## Conform to the Protocol

The `DSCameraController` property `delegate` expects an object that conforms to the protocol `DSCameraControllerDelegate`.  You must implement the protocol required methods in order to interact with the camera controller. Below, we give an example of such implementation for detecting a generic document.

```swift
#pragma mark -
#pragma mark - DSCameraControllerDelegate

- (void)cameraControllerDidCancel:(DSCameraController *)controller {

    [self dismissViewControllerAnimated:YES completion:nil];

}

- (void)cameraControllerWillStartProcessingImage:(DSCameraController *)controller {

    [controller showMessage:@"Processing Image"];

}

- (void)cameraController:(DSCameraController *)controller didFinishProcessingImageWithInfo:(NSDictionary *)info {

    NSError *error = [info objectForKey:DSCameraControllerInfoError];
    DSImage *image = [info objectForKey:DSCameraControllerInfoImage];
    DSDocumentRegion *region = [info objectForKey:DSCameraControllerInfoDocumentRegion];

    [controller showMessage:nil];
    
    if (error == nil && image != nil && region != nil) {

        // Do something with the image.

        [controller pauseCapture];

        [self dismissViewControllerAnimated:YES completion:nil];

    } else {
    
        // No good. The SDK will keep trying to capture a document.
    
    }

}
```



