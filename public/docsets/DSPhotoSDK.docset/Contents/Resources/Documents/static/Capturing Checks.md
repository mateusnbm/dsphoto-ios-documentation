# Capturing Checks

Learn how to capture high quality images and detect checks using the camera module.

## Present the Camera Controller

The camera module provides an interface for capturing high quality images and detecting documents in them. Start by implementing a function to present the view controller with the desired configuration parameters, see `DSCameraController` for the available properties. An example of such implementation is given below:

```swift
- (void)showCameraController {

    DSCameraController *controller = [[DSCameraController alloc] init];
    
    controller.delegate = self;
    controller.captureMode = DSCameraControllerCaptureModeAutomatic;
    controller.capturePreprocessMode = DSPreprocessModePrecise;
    controller.captureDocumentFormat = DSDocumentFormatBrazilianCheck;

    controller.overlayColor = [UIColor colorWithWhite:0 alpha:0.6];
    controller.overlayBordersColor = [UIColor grayColor];

    controller.shouldShowScoresView = YES;
    controller.ignoreScoresAfterTimeInterval = 10;

    controller.shouldIgnoreImageScores = NO;
    controller.acceptableLightValue = 0.6f;
    controller.acceptableFocusValue = 0.6f;
    controller.acceptableShineValue = 0.6f;
    controller.acceptableShadowValue = 0.6f;
    
    [self presentViewController:controller animated:true completion:nil];
    
}
```

## Conform to the Protocol

The `DSCameraController` property `delegate` expects an object that conforms to the protocol `DSCameraControllerDelegate`.  You must implement the protocol methods in order to interact with the camera controller. Below, we give an example of such implementation for detecting a check.

```swift
#pragma mark -
#pragma mark - DSCameraControllerDelegate

- (void)cameraControllerDidCancel:(DSCameraController *)controller {

    [self dismissViewControllerAnimated:YES completion:nil];

}

- (void)cameraControllerWillStartProcessingImage:(DSCameraController *)controller {

    [controller showMessage:@"Processing Image"];

}

- (void)cameraController:(DSCameraController *)controller didFinishProcessingImageWithInfo:(NSDictionary *)info {

    NSError *error = [info objectForKey:DSCameraControllerInfoError];
    DSImage *image = [info objectForKey:DSCameraControllerInfoImage];
    DSDocumentRegion *region = [info objectForKey:DSCameraControllerInfoDocumentRegion];
    DSOverlayDetectionStatus status = [[info objectForKey:DSCameraControllerInfoOverlayStatus] integerValue];

    [controller showMessage:nil];

    if (error == nil && image != nil && region != nil) {
    
        switch (status) {
        
            case DSOverlayDetectionStatusOK:
        
                // Do something with the image.
        
                [controller pauseCapture];
        
                [self dismissViewControllerAnimated:YES completion:nil];
        
                break;
            
            case DSOverlayDetectionStatusCloser:
            
                [controller showMessage:@"Move Closer"];
            
                break;
            
            case DSOverlayDetectionStatusNone:
            
                // No good. The SDK will keep trying to capture a document.
            
                break;
        
        }

    }

}
```



