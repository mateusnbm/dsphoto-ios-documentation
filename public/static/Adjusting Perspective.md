# Adjusting Perspective

Learn how to adjust the perspective of documents.  You can use the module `DSPerspectiveAdjustment` to perform a standalone adjustment when the document's region is known. The module `DSPerspectiveController` provides a visual interface to refine a region and then perform the adjustment.

## Standalone Adjustment

Use it to adjust the perspective without visual aids. Sample code below.

```swift
NSError *error;

DSDocumentType type = DSDocumentTypeOpenedCNH;
DSPreprocessMode mode = DSPreprocessModePrecise;

UIImage *tmp = [UIImage imageNamed:@"IMAGE_CONTAINING_A_DOCUMENT"];
DSImage *image = [[DSImage alloc] initWithImage:tmp];

DSDocumentDetector *detector = [DSDocumentDetector sharedInstance];
DSDocumentRegion *region = [detector detectDocumentRegionInImage:image withPreprocessMode:mode];

DSPerspectiveAdjustment *adjuster = [DSPerspectiveAdjustment sharedInstance];
DSImage *adjustedImage = [adjuster adjustPerspectiveForImage:image withRegion:region documentType:type error:&error];

if (error == nil) {

    // Do something with adjusted image.

} else {

    if (error.code == 1) {

        NSLog(@"Invalid region.");

    } else if (error.code == 2) {

        NSLog(@"Invalid region for the given document type.");

    }

}
```

## Present the Perspective Controller

The perspective controller module provices an interface for adjusting regions that denote documents. Start by implementing a function to present the view controller with the desired configuration parameters, see `DSPerspectiveController` for the available properties. An example of such implementation is given below:

```swift
- (void)showPerspectiveControllerWithImage:(DSImage *)image initialImageRegion:(DSDocumentRegion *)region {

    DSPerspectiveController *controller = [[DSPerspectiveController alloc] init];

    controller.delegate = self;
    controller.image = image;
    controller.initialRegion = region;
    controller.documentType = DSDocumentTypeOpenedCNH;

    controller.validColor = [UIColor greenColor];
    controller.invalidColor = [UIColor purpleColor];
    controller.zoomColor = [UIColor yellowColor];

    controller.shouldPresentDefaultMenu = YES;

    controller.magnifyingViewRadius = 40.0;
    controller.edgePointRadius = 9.0;

    [self presentViewController:controller animated:YES completion:nil];

}
```

## Conform to the Protocol

The `DSPerspectiveController` property `delegate` expects an object that conforms to the protocol `DSPerspectiveControllerDelegate`. You must implement the protocol methods in order to interact with the perspective controller. Below, we give an example of such implementation.

```swift
- (void)perspectiveControllerDidCancelAdjustment:(DSPerspectiveController *)controller {

    [self dismissViewControllerAnimated:YES completion:nil];

}

- (void)perspectiveControllerWillStartProcessingImage:(DSPerspectiveController *)controller {

    [self dismissViewControllerAnimated:YES completion:nil];

}

- (void)perspectiveController:(DSPerspectiveController *)controller didFinishProcessingImageWithInfo:(NSDictionary *)info {

    DSImage *adjustedImage = [info objectForKey:DSPerspectiveControllerInfoAdjustedImage];
    DSDocumentRegion *adjustedRegion = [info objectForKey:DSPerspectiveControllerInfoAdjustedRegion];

    if (adjustedImage == nil) {

        // Something went wrong while adjusting the perspective, try again.

    } else {

        // Do somehting with the adjusted image.

    }

}
```



