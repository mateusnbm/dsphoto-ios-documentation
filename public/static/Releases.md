# Releases

This document lists the major changes across releases and related resources.

## Version 2.0.0.0

Resources:

* [Demo App - Gitlab](https://gitlab.com/ldcinojosa/DSPhoto-sample-ios/tree/2.0.0)
* [Documentation - Web Version](static/releases/2.0.0.0/documentation_web.zip)
* [Documentation - PDF Version](static/releases/2.0.0.0/documentation_pdf.zip)
* [Templates](static/releases/2.0.0.0/templates.zip)

Release notes:

* The method `activate:` from the class `DSLicenseService` now returns a boolean.

* A new module was created, it is called `DSDocumentReader`. You can use it to extract textual information from pictures of documents, based on templates. Check out the guides `Reading Documents` and `Creating Templates` to learn more about it.

* The method `rotate:` from the class `DSImage` was updated. Now, it expects an unsigned integer specifying the number of degrees to rotate the image. It must be a multiple of 90º (ex: 90º, 180º, 270º).

* The method `rotate:boxSize:` of the class `DSDocumentRegion` was updated. Now, it expects an unsigned integer specifying the number of degrees to rotate the document region. It must be a multiple of 90º (ex: 90º, 180º, 270º).

* The perspective controller `DSPerspectiveController` now rotates images by 90º on each call to the method `rotateImage`.

* The constants used across the whole SDK were documented. Now, each one contains a brief description of what it represents. We recommend that you access them through their type definition.

## Version 1.7.0.0

Resources:

* [Demo App - Gitlab](https://gitlab.com/ldcinojosa/DSPhoto-sample-ios/tree/1.7.0)
* [Documentation - Web Version](static/releases/1.7.0.0/documentation_web.zip)
* [Documentation - PDF Version](static/releases/1.7.0.0/documentation_pdf.zip)

Release notes:

* A web version of the documentation was compiled for the first time. It is much more complete, bringing detailed descriptions of all modules and how they interact with each other. All guides were updated to match the new api and are being shipped in both formats, web and pdf. To use the web version, uncompress the folder `documentation.zip` and open the file `index.html` in the browser of your choice.

* Some methods of the protocol `DSCameraControllerDelegate` were combined and others had their signatures modified. Now, it's easier to interact with the class `DSCameraController`.

* Some methods of the protocol `DSPerspectiveControllerDelegate` were renamed and a new one was included to give you more control during the adjustment flow.

* The method `initWithLicenseKey:` of the class `DSLicenseService` was renamed to `activate:`.

* The module `DSPhotoAnalyzer` was renamed to `DSImageAnalyzer`. Some methods had their signatures modified, check out the class documentation.

* The util `DSPhotoScore` was renamed to `DSImageScore`.

* The method `processCheckWithImage:format:region:` of the class `DSCheckReader` had its signature modified to `processCheck:documentFormat:`.

* A new module was created, it is called `DSImagePickerController`. You can use it to select images from the camera roll. Check out the documentation and the guide `Importing Images` for more information about it.

## Version 1.6.3.1

Resources:

* [Demo App - Gitlab](https://gitlab.com/ldcinojosa/DSPhoto-sample-ios/tree/1.6.3.1)
* [Documentation - PDF Version](static/releases/1.6.3.1/documentation_pdf.zip)

Release notes:

* This version brought a few improvements to the class `DSCameraController`. The quality scores are now being calculated more times without major performmance penalties. This may speed up the capture flow and improve the overall quality of the images captured.

## Version 1.6.3.0

Resources:

* [Demo App - Gitlab](https://gitlab.com/ldcinojosa/DSPhoto-sample-ios/tree/1.6.3)
* [Documentation - PDF Version](static/releases/1.6.3.0/documentation_pdf.zip)

Release notes:

* The module `DSPhotoAnalyzer` now provides two new parameters to assess the quality of images. You can analyse the image focus, luminosity, brightness and shadow. These parameters range from 0 to 1, where 0 represents the worst scenario and 1 represents the ideal one.

* The class `DSCameraController` may use the new quality scores to optimize the capture of images, improving the chances of capturing high quality images. This feature can be modelled through the following parameters:

    * `acceptableLightValue`: Real number, ranging from 0 to 1. Specifies the minimum light (luminosity) threshold that an image must achieve before the module performs the capture.
    
    * `acceptableFocusValue`: Real number, ranging from 0 to 1. Specifies the minimum focus threshold that an image must achieve before the module performs the capture.
    
    * `acceptableShineValue`: Real number, ranging from 0 to 1. Specifies the minimum shine (brightness) threshold that an image must achieve before the module performs the capture.
    
    * `acceptableShadowValue`: Real number, ranging from 0 to 1. Specifies the minimum shadow threshold that an image must achieve before the module performs the capture.
      
    * `shouldIgnorePhotoScores`: Determines if the module should analyze the quality scores before capturing images.
    
* New visual elements were embedded into the `DSCameraController` to present the quality scores to the user. You may set the parameter `shouldShowScoresView` to `YES` in order to present a set of four circular progress views displaying the focus, luminosity, brightness and shadow scores.

* Another new visual element is a label that can be presented on the `DSCameraController` to show a message during a short amount of time. The message displays instructions about how to capture high quality images.

## Version 1.6.2.0

Resources:

* [Demo App - Gitlab](https://gitlab.com/ldcinojosa/DSPhoto-sample-ios/tree/1.6.2)
* [Documentation - PDF Version](static/releases/1.6.2.0/documentation_pdf.zip)

Release notes:

* Added two new properties to the class `DSPerspectiveController`.

    * `shouldDismissViewControllerOnCompletion`: Boolean variable that determines whether the view controller should be removed modally from the view controllers hierarchy. More precisely, if it is set to `YES`, the module will call the function `dismissViewControllerAnimated:completion:` when `cancelAdjustments` or `confirmAdjustments` is invoked.

    * `shouldAnimateViewControllerDismissal`: Boolean variable that determines if the view controller removal should be animated, only applies when `shouldDismissViewControllerOnCompletion` is set to `YES`.

## Version 1.6.1.0

Resources:

* [Documentation - PDF Version](static/releases/1.6.1.0/documentation_pdf.zip)

Release notes:

* Included two new methods to the protocol `DSCameraControllerDelegate`.

    * `cameraController:didStartProcessingImageInHighQuality:` Called when the camera module starts processing a new frame.
    
    * `cameraController:didFinishProcessingInHighQuality:` Called when the camera module finishes processing a new frame.

