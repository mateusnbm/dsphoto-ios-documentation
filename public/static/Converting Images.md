# Converting Images

You can use the module `DSImageConverter` to convert images to a given format and color space. See the sample code below for an usage example.

```swift
UIImage *tmp = [UIImage imageNamed:@"SOME_IMAGE"];
DSImage *image = [[DSImage alloc] initWithImage:tmp];

DSDocumentType type = DSDocumentTypeCNH;
DSImageColor color = DSImageColorBW;
DSImageFormat format = DSImageFormatTIFF;

DSImageConverter *converter = [DSImageConverter sharedInstance];
NSData *result = [converter convertImage:image containingDocument:type toColorSpace:color toFormat:format];
```


