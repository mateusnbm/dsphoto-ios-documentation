# Importing Images

Learn how import images from the photo library and detect documents automatically.

## Present the Image Picker Controller

The SDK provides a module for importing images from the device's photo library. It provides the covenience of running the document detector automatically. Start by implementing a function to present the view controller with the desired configuration parameters, see `DSImagePickerController` for the available properties. An example of such implementation is given below:

```swift
- (void)selectImageFromCameraRoll {

    self.imagePickerController = [[DSImagePickerController alloc] init];
    self.imagePickerController.delegate = self;

    [self presentViewController:self.imagePickerController.presentableViewController animated:YES completion:nil];

}
```

## Conform to the Protocol

The `DSImagePickerController` property `delegate` expects an object that conforms to the protocol `DSImagePickerControllerDelegate`.  You must implement the protocol methods in order to interact with the camera controller. Below, we give an example of such implementation for detecting a generic document.

```swift
#pragma mark -
#pragma mark - DSImagePickerControllerDelegate

- (void)customImagePickerControllerDidCancel:(DSImagePickerController *)controller {

    [self dismissViewControllerAnimated:YES completion:nil];

}

- (void)customImagePickerControllerWillStartProcessingImage: (DSImagePickerController *)controller {

    [self dismissViewControllerAnimated:YES completion:nil];

}

- (void)customImagePickerController:(DSImagePickerController *)controller didFinishPickingImageWithInfo:(NSDictionary *)info {

    DSImage *image = [info objectForKey:DSImagePickerControllerInfoImage];
    DSDocumentRegion *region = [info objectForKey:DSImagePickerControllerInfoDocumentRegion];

    if (image != nil && region != nil) {

        // Use the image and region.

    }

}
```



