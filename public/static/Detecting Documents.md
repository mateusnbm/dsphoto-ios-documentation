# Detecting Documents

The SDK provides a standalone module to detect documents in images, the `DSDocumentDetector`. See the sample code below for an usage example.

```swift
CGFloat ratio = 0.1;
UIImage *tmp = [UIImage imageNamed:@"IMAGE_CONTAINING_A_DOCUMENT"];
DSImage *image = [[DSImage alloc] initWithImage:tmp];
DSPreprocessMode mode = DSPreprocessModePrecise;

DSDocumentDetector *detector = [DSDocumentDetector sharedInstance];
DSDocumentRegion *region = [detector detectDocumentRegionInImage:image withPreprocessMode:mode minimumDocumentAreaRatio:ratio];

if (region == nil) {

    // No documents detected.


} else {

    // Detected a document.

}
```
