# Analyzing Images

You can use the module `DSImageAnalyzer` to analyze the quality of an image. It can compute four quality scores that reflect the quality of the focus, shadow, lightness and shining. See the sample code below for an usage example.

```swift
UIImage *tmp = [UIImage imageNamed:@"SOME_IMAGE"];
DSImage *image = [[DSImage alloc] initWithImage:tmp];

DSImageAnalyzer *analyzer = [DSImageAnalyzer sharedInstance];
DSImageScore *scores = [analyzer calculateImageScores:image];

NSLog(@"scores %f %f %f %f", scores.luminosityScore, scores.focusScore, scores.shineScore, scores.shadowScore);
```


