# Reading Checks

Learn how to extract the CMC7 from checks. The SDK provides the standalone module `DSCheckReader` for this purpose. See the sample code below for an usage example.

```swift
UIImage *tmp = [UIImage imageNamed:@"IMAGE_CONTAINING_A_CHECK"];
DSImage *image = [[DSImage alloc] initWithImage:tmp];

DSDocumentFormat format = DSDocumentFormatBrazilianCheck;

DSCheckReader *reader = [DSCheckReader sharedInstance];
DSCheckData *data = [reader processCheck:image documentFormat:format];

if (data != nil && data.valid == YES && [data.result isEqualToString:@"?"] == NO) {

    NSLog(@"CMC7: %@", data.result);

}
```




